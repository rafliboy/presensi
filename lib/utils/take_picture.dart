import 'package:image_picker/image_picker.dart';

class TakePicture{
  TakePicture._();

  static final ImagePicker _picker = ImagePicker();

  static Future<dynamic> take() async {
    final XFile? image = await _picker.pickImage(
        source: ImageSource.camera,
        preferredCameraDevice: CameraDevice.front);

    if (image != null) {
      return image;
    }

    return false;
  }
}