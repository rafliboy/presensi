import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class KoneksiPutus extends StatelessWidget {
  const KoneksiPutus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: Column(
          children: [
            Padding(padding: EdgeInsets.only(top: 100)),
            const Text(
              "Tidak ada Koneksi Internet!!",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30),
            ),
            const SizedBox(
              height: 60,
            ),
            Lottie.asset('assets/noconnection.json',fit: BoxFit.fill),
          ],
        ),
      )),
    );
  }
}
