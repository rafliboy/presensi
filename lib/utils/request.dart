import 'package:http/http.dart' as http;

class Request {
  Request._();

  // define base url, mempermudah saat pindah server atau domain diganti
  static const String baseUrl =
      'https://login.rosalyne.co.id/sysworkflow/en/neoclassic/62554682161babf18a1a7c3094935448/';

  static Future<bool> doPresence({
    String? image,
    String? uid,
    String? username,
    var lat,
    var long,
    bool pulang = false,
  }) async {
    // define endpoint api
    String endpoint = 'presensi_masuk_base64.php';
    if (pulang){
      endpoint = 'presensi_pulang_base64.php';
    }

    bool _isSuccess = false;
    await http.post(Uri.parse(baseUrl + endpoint), body: {
      "foto": image,
      "user_uid": uid,
      "username": username,
      "latitude": lat,
      "longitude": long,
    }).then((res) {
      print("RESULT : ${res.statusCode}");
      if (res.statusCode == 200){
        _isSuccess = true;
      }
    }).catchError((err) {
      print(err);
    });

    return _isSuccess;
  }
}
