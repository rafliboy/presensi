import 'package:Presensi/model/location_service.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart' as loc;

class GetLocation {
  GetLocation._();

  /*
  class ini digunakan untuk mengecek izin lokasi dan service,
  */

  static Future<dynamic> start({required BuildContext context, bool isSkip = false,}) async {
    loc.Location _location = loc.Location();
    bool _isOn = await _location.requestService();
    loc.PermissionStatus _isAllow = await _location.requestPermission();
    if (_isOn && _isAllow == loc.PermissionStatus.granted) {
      return true;
    } else {
      if (!isSkip){
        CoolAlert.show(
          context: context,
          type: CoolAlertType.warning,
          text: "Location permission needed!",
          loopAnimation: true,
          barrierDismissible: false,
          onConfirmBtnTap: () => SystemNavigator.pop(),
        );
      }
    }
    return false;
  }

  static Future<dynamic> get({required BuildContext context, bool backOnConfirm = false}) async {
    loc.Location _location = loc.Location();
    loc.LocationData _locData;
    _locData = await _location.getLocation();
    print("locdata ${_locData}");
    UserLocation.lat = _locData.latitude!;
    UserLocation.long = _locData.longitude!;
    UserLocation.isMock = _locData.isMock!;
    if (_locData.isMock!) {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.warning,
        text: "Fake GPS Detection, Please turn OFF Mocklocation",
        loopAnimation: true,
        barrierDismissible: false,
        onConfirmBtnTap: backOnConfirm ? () => SystemNavigator.pop() : null,
      );
      return true;
    } else {
      return false;
    }
  }
}
