import 'dart:convert';
import 'package:Presensi/model/user.dart';
import 'package:Presensi/model/user_history.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class RiwayatList extends StatefulWidget {
  const RiwayatList({Key? key, this.date = ''}) : super(key: key);

  final String date;
  @override
  _RiwayatListState createState() => _RiwayatListState();
}

class _RiwayatListState extends State<RiwayatList> {
  Future<List<Riwayat>?> cekRiwayat() async {
    final pref = await SharedPreferences.getInstance();
    final userid = pref.getString('userid');

//String uid= "1670736205f029633d971a0003100970";
    String baseurl =
        "https://login.rosalyne.co.id/sysworkflow/en/neoclassic/62554682161babf18a1a7c3094935448/history-presensi.php?";

    String endpoint = "user_uid=$userid";

    print(endpoint);
    final response = await http.get(Uri.parse(baseurl + endpoint));
    print(baseurl + endpoint);

    if (response.statusCode == 200) {
      return riwayatFromJson(response.body);
    } else {
      print("eror ${response.toString()}");
      return null;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: FutureBuilder<List<Riwayat>?>(
        future: cekRiwayat(),
        builder: (context, AsyncSnapshot<List<Riwayat>?> snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error.toString());
            return Center(
              child: Text("eror ${snapshot.error.toString()}"),
            );
          } else if (snapshot.hasData) {
            List<Riwayat>? _user = [];
            // cek apakah ada tanggal dikirim dari dashboard
            if (widget.date != '') {
              // lakukan looping jika ada tanggal
              snapshot.data?.forEach((element) {
                // cek jika tanggal yang ada di snapshot.data sama dengan data yang diberikan dari dashboard
                if (element.tgl == widget.date) {
                  // jika sama data akan dimasukkan kedalam _user
                  _user?.add(element);
                }
              });
            } else {
              // jika tidak ada tanggal seluruh snapshot dimasukkan kedalam user
              _user = snapshot.data;
            }
            // biasakan gunakan {} saat menggunakan if dan else
            // boleh menggunakan if(......) jika tidak ada else
            // ---------------------------------------
            // tambahan validasi _user.isNotEmpty, dikarenakan kondisi yang paling memungkinkan adalah
            // user kosong, bukan user == null, tapi gpp pake juga untuk jaga jaga
            if (_user != null && _user.isNotEmpty) {
              return buildListView(_user);
            } else {
              return Text("kosong");
            }
          } else
            return CircularProgressIndicator();
        },
      ),
    );
  }

  Widget buildListView(List<Riwayat> user) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: ListView.builder(
                  padding: EdgeInsets.all(10),
                  // cek jika tanggal dari dashboard (widget.date) tidak kosong, itemCount nya menggunakan user.length
                  // jika date kosong, maka cek terlebih dahulu user.length, jika nilainya lebih dari 10, maka batasi list
                  // jangan lebih dari 10, jika kurang dari 10 tampilkan seluruh data
                  itemCount: widget.date != ''
                      ? user.length
                      : user.length > 10
                          ? 10
                          : user.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      child: ListTile(
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(user[index].foto),
                        ),
                        title: Text(
                            'Status: ${user[index].stats}\nPukul : ${user[index].jam}, Tanggal : ${user[index].tgl}'),
                        subtitle: Text("Alamat : ${user[index].alamat}"),
                      ),
                      color: Color(0xffDEF3F4),
                      elevation: 8,
                      shape: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(color: Color(0xffDEF3F4))),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
