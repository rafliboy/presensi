// ignore_for_file: avoid_unnecessary_containers

import 'dart:convert';
import 'dart:io';
import 'package:Presensi/home/attendance_result.dart';
import 'package:Presensi/home/riwayat.dart';
import 'package:Presensi/model/location_service.dart';
import 'package:Presensi/model/user.dart';

import 'package:Presensi/utils/get_location.dart';
import 'package:Presensi/utils/noconnection.dart';
import 'package:Presensi/utils/request.dart';
import 'package:Presensi/utils/take_picture.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';

import 'package:geocoding/geocoding.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:slide_digital_clock/slide_digital_clock.dart';
import 'package:intl/intl.dart';



class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  User?
      user; //buat menyimpan data user yg diambil dari prefs & utk dikirim ke halaman masuk
  // ignore: unused_field
  XFile? _imageFile;

  String address = '';

  DateTime selectedDate = DateTime.now();

  // kondisi awal riwayat tidak mengirimkan data tanggal
  Widget riwayat = Container();

  dynamic subscription;

  // ini digunakan untuk mengecek kondisi apakah awal dijalankan
  bool initial = true;

  void pickDate() {
    showDatePicker(
            context: context,
            initialDate: selectedDate,
            firstDate: DateTime(DateTime.now().year - 5),
            lastDate: DateTime.now())
        .then((value) {
      if (value != null) {
        setState(() {
          selectedDate = value;
          // saat tanggal dipilih maka panggil ulang riwayat dengan mengirimkan tanggal (String)
          // jangan lupa untuk di format date dulu, agar sesuai dengan format response api
          riwayat = RiwayatList(date: DateFormat('dd-MM-yyyy').format(value));
        });
      }
    });
  }

  @override
  void initState() {
    ambildata();
    // getLocation hanya dipanggil ketika absen saja, menghindari izin belum didapatkan
    // dari splash
    // getLocation();
    checkConnection();
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
    // pastikan cancel conectivity
    subscription.cancel();
  }

  checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      Navigator.push(context, MaterialPageRoute(builder: (context)=>const KoneksiPutus()));
      setState(() {
        initial = false;
      });
    } else {
      setState(() {
        riwayat = RiwayatList();
      });
    }
    checkConnectionChange();
  }

  checkConnectionChange() {
    // dapatkan perubahan status koneksi internet
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      // saat pertama kali dijalankan hanya tampilkan peringatan jika tidak ada koneksi
      print("RESULT : $initial");
      // cek jika terhubung ke data atau wifi
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        // auto refresh jika dari awal tidak ada koneksi
        setState(() {
          riwayat = RiwayatList();
        });
        if (initial) {
          setState(() {
            initial = false;
          });
        } else {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>const Dashboard()));
        }
      } else {
        
       Navigator.push(context, MaterialPageRoute(builder: (context)=>const KoneksiPutus()));
      }
    });
  }


  ambildata() async {
    final prefs = await SharedPreferences.getInstance();
    final res = prefs.getString('user');
    final resi = prefs.getString('userid');
    print(resi);
    if (res != null) {
      // kenapa harus pake kondisi? krn hasilnya bisa jadi null ketika halaman dasboard ini dibuka sebelum login
      setState(() {
        user = User.fromJson(jsonDecode(res));
        
      });
    }
  }

  _logOut() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('sLogin', false);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }

//fungsi untuk mengirim data ke API
  void doPresence(XFile image, {bool pulang = false}) async {
    loading();
    getLocation();
    String time = DateFormat("dd MMMM yyyy HH:mm").format(DateTime.now());
    final base64Image = File(image.path).readAsBytesSync();
    String _img64 = base64Encode(base64Image);
    print("${UserLocation.lat}, ${UserLocation.long}");
    var latitude = UserLocation.lat.toString();
    var longitude = UserLocation.long.toString();
    bool success = await Request.doPresence(
      image: _img64,
      uid: user?.userUid,
      username: user?.username,
      lat: latitude,
      long: longitude,
      pulang: pulang,
    );
    print(success);
    if (success) {
      Navigator.pop(context);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => AttendanceResult(
            name: "${user?.username}",
            time: time,
            location: address,
            image: File(image.path),
            // cek kondisi tipe Presensi pulang atau masuk
            type: pulang ? "keluar" : "masuk",
          ),
        ),
      );
    } else {
      Navigator.pop(context);
    }
  }

//fungsi asyncronus untuk mendapatkan lokasi user
  Future<void> getLocation() async {
    List<Placemark> placemark = await placemarkFromCoordinates(
        UserLocation.lat, UserLocation.long,
        localeIdentifier: 'en');
    setState(() {
      address =
          "${placemark[0].country!}, ${placemark[0].name!}, ${placemark[0].street!}, ${placemark[0].postalCode!}";
    });
    print(address);
  }

  loading() {
    AlertDialog alert = AlertDialog(
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CircularProgressIndicator(),
          Container(
            margin: const EdgeInsets.only(left: 20),
            child: const Text("Mohon Menunggu..."),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(
          'assets/appbar_logo.png',
          fit: BoxFit.contain,
          height: 50,
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              _logOut();
            }, //action logout
            child: Container(
              margin: EdgeInsets.only(right: 20),
              child: Icon(
                Icons.power_settings_new_sharp,
                size: 40,
                color: Colors.red,
              ),
            ),
          )
        ],
        backgroundColor: const Color(0xffDEF3F4),
        elevation: 0,
      ),
      body: SafeArea(
        child: Stack(
          children: [
            ClipPath(
              clipper: ClipPathClass(),
              child: Container(
                height: 250,
                width: 600,
                color: const Color(0xffDEF3F4),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 30),
              child: Column(
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        // jangan kasih height kalau didalam container ada column
                        // height: 150,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black.withOpacity(0.4),
                              blurRadius: 4,
                              offset: const Offset(0, 4),
                            ),
                          ],
                        ),
                        child: Container(
                          margin: EdgeInsets.fromLTRB(10, 20, 0, 10),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: [
                                  Align(
                                    alignment: AlignmentDirectional.topStart,
                                  ),
                                  Image.asset(
                                    "assets/card_logo.png",
                                    fit: BoxFit.contain,
                                    height: 100,
                                  ),
                                  SizedBox(width: 10),
                                  Column(
                                    children: [
                                      Text(
                                        "Hai.., ",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontStyle: FontStyle.italic),
                                      ),
                                      Text(
                                        "${user?.username}",
                                        style: TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      DigitalClock(
                                        areaWidth: 190,
                                        is24HourTimeFormat: true,
                                        areaDecoration: BoxDecoration(
                                          color: Colors.transparent,
                                        ),
                                        hourMinuteDigitTextStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 50,
                                        ),
                                      ),
                                      Text(
                                          DateFormat('EEEE, dd MMMM y')
                                              .format(DateTime.now()),
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontStyle: FontStyle.italic))
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Card(
                              child: InkWell(
                                onTap: () async {
                                  // sebelum melakukan absen cek lokasi terlebih dahulu,
                                  // tidak bisa absen jika lokasi tidak ada/tidak diberikan izin.
                                  // dengan seperti ini lokasi selalu diperbarui meskipun kondisi onResume

                                  bool isPermit =
                                      await GetLocation.start(context: context);
                                  // jika dapat izin baru ambil lokasi
                                  if (isPermit) {
                                    bool isMock = await GetLocation.get(context: context);
                                    if (!isMock){
                                      dynamic image = await TakePicture.take();
                                      if (image != false) {
                                        doPresence(image);
                                      }
                                    }
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  width: 130,
                                  height: 70,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Presensi",
                                        style: TextStyle(
                                          fontSize: 15,
                                        ),
                                      ),
                                      RichText(
                                          text: TextSpan(
                                        text: "DATANG",
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.redAccent,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ))
                                    ],
                                  ),
                                ),
                              ),
                              elevation: 8,
                              shadowColor: Color(0xff36CCCA),
                              shape: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide:
                                    BorderSide(color: Color(0xffDEF3F4)),
                              ),
                            ),
                            Card(
                              child: InkWell(
                                onTap: () async {
                                  

                                  bool isPermit =
                                      await GetLocation.start(context: context);
                                  // jika dapat izin baru ambil lokasi
                                  if (isPermit) {
                                    bool isMock = await GetLocation.get(context: context);
                                    if (!isMock){
                                      dynamic image = await TakePicture.take();
                                      if (image != false) {
                                        // tambahkan pulang: true, untuk menandakan bahwa ini adalah absen pulang
                                        doPresence(image, pulang: true);
                                      }
                                    }
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  width: 130,
                                  height: 70,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Presensi",
                                        style: TextStyle(
                                          fontSize: 15,
                                        ),
                                      ),
                                      RichText(
                                          text: TextSpan(
                                        text: "PULANG",
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Colors.redAccent,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ))
                                    ],
                                  ),
                                ),
                              ),
                              elevation: 8,
                              shadowColor: Color(0xff36CCCA),
                              shape: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: BorderSide(
                                  color: Color(0xffDEF3F4),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    SizedBox(width: 5),
                    Text(
                      "RIWAYAT KEHADIRAN",
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff36CCCA)),
                    )
                  ]),
                  Container(
                    height: 3,
                    color: Color(0xffDEF3F4),
                  ),
                  Expanded(
                    child: Container(
                      //color: Colors.purple,
                      child: Column(
                        children: [
                          Container(
                            width: double.infinity,
                            height: 45,
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            color: Color(0xff36CCCA),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.date_range_outlined,
                                  color: Colors.white,
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Text(
                                    DateFormat('dd MMMM y')
                                        .format(selectedDate),
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.white)),
                                const Spacer(),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white, // background
                                    onPrimary: Color(0xff36CCCA),
                                  ),
                                  onPressed: () {
                                    pickDate();
                                  },
                                  child: Text(
                                    "pilih tanggal",
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          //body
                          Expanded(
                            child: NestedScrollView(
                              headerSliverBuilder: (BuildContext context,
                                  bool innerBoxIsScrolled) {
                                return <Widget>[];
                              },
                              // gunakan variable Widget untuk body, tujuannya agar body bisa ter refresh setiap setState
                              body: riwayat,
                            ),
                          )
                          //navigation
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ClipPathClass extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height - 60);
    path.quadraticBezierTo(
      size.width / 2,
      size.height,
      size.width,
      size.height - 60,
    );
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
