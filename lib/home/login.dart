import 'dart:convert';

import 'package:Presensi/model/user.dart';
import 'package:Presensi/model/user_history.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'dashboard.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var userController = TextEditingController();
  var passController = TextEditingController();
  bool _isObscure = true;

  @override
  void initState() {
    super.initState();
  }

  void cekLogin() async {
    if (userController.text.isNotEmpty && passController.text.isNotEmpty) {
      var response = await http.post(
          Uri.parse(
              'https://login.rosalyne.co.id/sysworkflow/en/neoclassic/62554682161babf18a1a7c3094935448/login_presensi.php'),
          body: {
            'username': userController.text,
            'password': passController.text
          });

      if (response.statusCode == 200) {
        // jika dengan model
        User user = User.fromJson(jsonDecode(response.body)[0]);
        
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (_) =>
                  Dashboard(), //ini kenapa tanpa const kayak tadi tapi bisa muncul di class dashboard?
              // kan data usernya disimpan di prefs
            ));
        SharedPreferences prefs = await SharedPreferences.getInstance();
        setState(() {
          prefs.setBool('sLogin', true);
          prefs.setString(
              'user', jsonEncode(user));
               prefs.setString(
              'userid', user.userUid);
              
              // simpan user dalam bentuk json string
        });
        // kalau tanpa model
        // Navigator.pushReplacement(
        //   context,
        //   MaterialPageRoute(
        //       builder: (_) => Dashboard(
        //             hasil: jsonDecode(response.body)[0]['username'],
        //           )),
        // );

        //print("TES = ${user.userUid}");
      } else {
        print('error ${response.body}');
        return null;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff36CCCA),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10, 100, 10, 80),
        child: Center(
          child: Container(
            height: 550,
            width: 350,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                Image.asset(
                  "assets/logo.png",
                  scale: 2,
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  width: 250,
                  child: TextField(
                    controller: userController,
                    decoration: InputDecoration(
                      //border: InputBorder.none,
                      labelText: 'username',
                      suffixIcon: Icon(Icons.people_alt),
                    ),
                  ),
                ),
                Container(
                  width: 250,
                  child: TextField(
                    obscureText: _isObscure,
                    controller: passController,
                    decoration: InputDecoration(
                      labelText: 'password',
                      suffixIcon: IconButton(
                        icon: Icon(
                            _isObscure ? Icons.visibility : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                InkWell(
                  child: Container(
                    width: 200,
                    padding: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      color: Color(0xff36CCCA),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey.shade200,
                            offset: Offset(2, 4),
                            blurRadius: 5,
                            spreadRadius: 2)
                      ],
                    ),
                    child: Text(
                      'Login',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                  ),
                  //onTap: cekLogin,
                  onTap: () {
                    cekLogin();
                  },
                ),
              ],
            ),
          ),
        ),
        ),
      
    );
  }
}
