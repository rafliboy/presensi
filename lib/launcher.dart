import 'dart:async';
import 'package:Presensi/utils/get_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:Presensi/home/login.dart';
import 'package:Presensi/home/dashboard.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LauncherPage extends StatefulWidget {
  const LauncherPage({Key? key}) : super(key: key);

  @override
  _LauncherPageState createState() => _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage> {
  @override
  // ignore: must_call_super
  void initState() {
    locationService();
  }

  // penamaan function/method diawali dengan huruf kecil (camelCase)
  void dialog() {
    CoolAlert.show(
      context: context,
      type: CoolAlertType.warning,
      text: "Fake GPS Detection, Please turn OFF Mocklocation",
      loopAnimation: true,
      barrierDismissible: false,
      onConfirmBtnTap: () => SystemNavigator.pop(),
    );
  }

  void locationService() async {
    // cek terlebih dahulu izin
    // isSkip digunakan untuk disable alert dan membiarkan tetep masuk,
    bool isPermit = await GetLocation.start(
      context: context,
      isSkip: true,
    );
    // jika dapat izin baru ambil lokasi
    if (isPermit) {
      // tambahan backOnConfirm agar ketika klik ok user keluar dari aplikasi
      bool isMock =
          await GetLocation.get(context: context, backOnConfirm: true);
      if (!isMock) {
        startLaunching();
      }
    } else {
      // jika tidak dapat izin biarkan masuk, ketika akan absen baru minta lagi
      startLaunching();
    }
  }

  Future startLaunching() async {
    final prefs = await SharedPreferences.getInstance();
    bool sLogin;
    sLogin = prefs.getBool('sLogin') ?? false;
    var duration = const Duration(seconds: 1);
    return Timer(duration, () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
        return sLogin ? const Dashboard() : const LoginPage();
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Center(
            child: Image.asset(
              'assets/splash.png',
              height: MediaQuery.of(context).size.height,
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}
