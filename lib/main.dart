import 'package:Presensi/home/login.dart';
import 'package:Presensi/home/dashboard.dart';
import 'package:Presensi/launcher.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
   
  }

  // Future<dynamic> permission() async {
  //   PermissionStatus _permissionLocation;
  //   Location location = Location();
  //   bool _serviceEnabled;
  //   _permissionLocation = await location.hasPermission();
  //   if (_permissionLocation == PermissionStatus.denied) {
  //     _permissionLocation = await location.requestPermission();
  //     if (_permissionLocation != PermissionStatus.granted) {
  //       // pastikan permission di dapatkan terlebih dahulu, baru cek servis
  //       _serviceEnabled = await location.serviceEnabled();
  //       if (!_serviceEnabled) {
  //         _serviceEnabled = await location.requestService();
  //         if (!_serviceEnabled) {
  //           return;
  //         }
  //       }
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LauncherPage(),
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => const LoginPage(),
        '/dashboard': (BuildContext context) => Dashboard(),
      },
    );
  }
}
