// To parse this JSON data, do
//
//     final riwayat = riwayatFromJson(jsonString);

import 'dart:convert';



List<Riwayat> riwayatFromJson(String str) =>
    List<Riwayat>.from(json. decode(str).map((x) => Riwayat.fromJson(x)));

String riwayatToJson(List<Riwayat> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Riwayat {
  Riwayat({
    required this.userUid,
    required this.username,
    required this.tgl,
    required this.jam,
    required this.stats,
    required this.lstats,
    required this.foto,
    required this.alamat,
    required this.warna,
    required this.lalamat,
  });

  String userUid;
  String username;
  String tgl;
  String jam;
  String stats;
  String lstats;
  String foto;
  String alamat;
  String warna;
  String lalamat;

  factory Riwayat.fromJson(Map<String, dynamic> json) => Riwayat(
        userUid: json["user_uid"],
        username: json["username"],
        tgl: json["tgl"],
        jam: json["jam"],
        stats: json["stats"],
        lstats: json["lstats"],
        foto: json["foto"],
        alamat: json["alamat"],
        warna: json["warna"],
        lalamat: json["lalamat"],
      );

  get http => null;

  Map<String, dynamic> toJson() => {
        "user_uid": userUid,
        "username": username,
        "tgl": tgl,
        "jam": jam,
        "stats": stats,
        "lstats": lstats,
        "foto": foto,
        "alamat": alamat,
        "warna": warna,
        "lalamat": lalamat,
      };

  


 

 
}
